def greater_than_zero(number):
    if number > 0:
        return True
    else:
        return False

# if that returns true when true and false when false, you don't actually have to write an if
# number > 0 is boolean value
# can write one-line function (def line doesn't count)

def greater_than_zero(number):
    return number > 0

# Write the less_than function, here, as a
# simple one-line function
def less_than(left, right):
    if left < right:
        return True
    else:
        return False


# Write the less_than function, here, as a
# simple one-line function

def less_than(left, right):
    return left < right


# Write the maximum function, here
def maximum(num1, num2):
    if num1 >= num2:
        return num1
    else:
        return num2

# Write the longest function, here
def longest(string1, string2):
    if len(string1) >= len(string2):
        return string1
    else:
        return string2

# Write the greater_than_zero function, here
def greater_than_zero(number):
    return number > 0

# Write the longest function, here
def longest(string1, string2):
    if len(string1) >= len(string2):
        return string1
    else:
        return string2

# Write the is_empty function, here, as a
# simple one-line function
def is_empty(s):
    return not s


# Write the low_med_high function, here
def low_med_high(num):
    if num < 10:
        return "low"
    elif num < 100:
        return "medium"
    else:
        return "high"


# Write the add_numbers function, here
def add_numbers(numbers):
    total = 0
    for number in numbers:
        total = total + number
    return total


# Write the add_numbers function, here
def add_numbers(numbers):

    sum = 0

    for number in numbers:
        sum += number

    return sum


# Write the multiply_numbers function, here
def multiply_numbers(numbers):

    product = 1

    for number in numbers:
        product *= number

    return product


# Write the maximum function, here
def maximum(numbers):

    max = 0

    for number in numbers:
        if number > 0:
            max = number

    return max


# Write the count_greater_than_100 function, here
def count_greater_than_100(numbers):

    count = 0

    for number in numbers:
        if number > 100:
            count += 1

    return count

# Write the count_greater_than function, here
def count_greater_than(numbers, lower_limit):

    count = 0

    for number in numbers:
        if number > lower_limit:
            count += 1

    return count


# Write the between function, here
def between(numbers, lower_limit, upper_limit):

    count = 0

    for number in numbers:
        if number > lower_limit and number < upper_limit:
            count += 1

    return count


# Write the third_item function, here
def third_item(values):
    if len(values) < 3:
        return None
    else:
        return values[2]


# Write the longest function, here
def longest(strings):

    length = 0
    winner = ""

    for string in strings:
        if len(string) > length:
            length = len(string)
            winner = string

    return winner


# Write the less_than_10 function, here
def less_than_10(numbers):
    new_list = []
    for number in numbers:
        if number < 10:
            new_list.append(number)
    return new_list


# Write the gte_10 function, here
def gte_10(numbers):

    greats = []

    for number in numbers:
        if number >= 10:
            greats.append(number)
    return greats


# Write the only_teens function, here
def only_teens(numbers):

    teens = []

    for number in numbers:
        if number >= 13 and number <= 19:
            teens.append(number)
    return teens


# Write the strings_to_integers function, here
def strings_to_integers(strings):

    ints = []

    for string in strings:
        ints.append(int(string))
    return ints
